<div id="extra">
<div style="display: block;" class="post-it curl" id="post-it">
<div class="wrapper">
<div class="content handwritten">
<h2 class="attn handwritten">To Do:</h2>
<ul id="postItList">
<li class="guided-complete">
  <span class="guided-wrap">
    <span style="background-position: 750px 50%;">Update your education</span>
  </span>
</li>
<li class="guided-complete">
  <span class="guided-wrap">
    <span style="background-position: 750px 50%;">Add your skills &amp; expertise</span>
  </span>
</li>
<li class="guided-active">
  <span class="guided-wrap">
    <span style="background-position: 750px 50%;">Add more details</span>
  </span>
</li>
<li>
  <span class="guided-wrap">
    <span>Add languages</span>
  </span>
</li>
<li>
  <span class="guided-wrap">
    <span>Add your projects</span>
  </span>
</li>
</ul>
<span class="guided-result" style="display: block;">Showcase your top accomplishments</span>
</div>
</div>
</div>
</div>
